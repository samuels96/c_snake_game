default: build

build:
	gcc -o snake src/snake_game.c src/snake.c src/arena.c src/fruit.c -lncurses

run: build
	./snake

