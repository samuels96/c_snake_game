#include "snake.h"
#include <ncurses.h>

void snake_init(struct Snake *snake, int x_bound, int y_bound){
    snake->dir = 0;
    snake->len = 1;
    snake->body[0][0] = x_bound / 2;
    snake->body[0][1] = y_bound / 2;
    for(int i = 1; i < 100; i += 1){
        for(int j = 0; j < 2; j += 1){
            snake->body[i][j] = 0;
        }
    }
}

void snake_print(struct Snake *snake){
    for(int i = 0; i < snake->len; i += 1){
        mvaddch(snake->body[i][1], snake->body[i][0], ACS_DIAMOND);
    };
}

void snake_print_crash(struct Snake *snake){
    int *snake_xy = snake_get_head_ptr(snake);
    mvaddch(snake_xy[1], snake_xy[0], ACS_PLMINUS);
}

int *snake_get_head_ptr(struct Snake *snake){
    return snake->body[snake->len-1];
}

int snake_set_head(struct Snake *snake, int x, int y){
    snake->body[snake->len-1][0] = x;
    snake->body[snake->len-1][1] = y;
}

int snake_grow(struct Snake *snake, int x, int y){
    snake->len += 1;
    snake_set_head(snake, x, y);
}

int *snake_get_next_pos(struct Snake *snake, enum Direction dir){
    static int next_pos[2];

    int *snake_head = snake_get_head_ptr(snake);
    next_pos[0] = snake_head[0];
    next_pos[1] = snake_head[1];

    switch(dir){
        case UP:
            next_pos[1] -= 1;
            break;
        case DOWN:
            next_pos[1] += 1;
            break;
        case LEFT:
            next_pos[0] -= 1;
            break;
        case RIGHT:
            next_pos[0] += 1;
            break;
    }

    return next_pos;
}


char eval_next_move(int x, int y){
    chtype next_char = mvinch(y, x);

    if(next_char == ACS_DIAMOND){
        return -1;
    }
    else if(next_char == ACS_BLOCK){
        return 1;
    }
    else if(next_char == ACS_CKBOARD){
        return 2;
    }
    else{
        return 0;
    }
}

int snake_move(struct Snake *snake, enum Direction dir, int x_bound, int y_bound){
    
    int ret_code = 0;

    int *next_pos = snake_get_next_pos(snake, dir);

    for(int i = 0; i < snake->len; i +=1){
        for(int j = 0; j < 2; j += 1){
            snake->body[i][j] = snake->body[i+1][j];
        }
    }

    ret_code = eval_next_move(next_pos[0], next_pos[1]);

    if(ret_code != 2){
        snake_set_head(snake, next_pos[0], next_pos[1]);

        if(ret_code == 1){
            snake_grow(snake, next_pos[0], next_pos[1]);
        }
    }

    if(ret_code == 2){
        if(next_pos[0] == 0){
                snake_set_head(snake, x_bound-1, next_pos[1]);
        }
        else if(next_pos[0] == x_bound){
                snake_set_head(snake, 1, next_pos[1]);
        }

        if(next_pos[1] == 0){
                snake_set_head(snake, next_pos[0], y_bound-1);
        }
        else if(next_pos[1] == y_bound){
                snake_set_head(snake, next_pos[0], 1);
        }
    }

    return ret_code;
}
