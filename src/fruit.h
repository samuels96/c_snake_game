struct Fruit{
    int pts;
    int pos[2];
};

struct Fruit fruit_factory(int pts, int x, int y);
int fruit_init(struct Fruit *fruit, int x, int y);
int fruit_print(struct Fruit *fruit);
