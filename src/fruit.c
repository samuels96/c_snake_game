#include "fruit.h"
#include <ncurses.h>

struct Fruit fruit_factory(int pts, int x, int y){
    struct Fruit fruit;
    fruit_init(&fruit, x, y);
    return fruit;
}

int fruit_init(struct Fruit *fruit, int x, int y){
    fruit->pts = 10;
    fruit->pos[0] = x;
    fruit->pos[1] = y;
}

int fruit_print(struct Fruit *fruit){
    mvaddch(fruit->pos[1], fruit->pos[0], ACS_BLOCK);
}
