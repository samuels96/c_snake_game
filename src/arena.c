#include "arena.h"
#include <ncurses.h>

void arena_init(struct Arena *arena, int width, int height){
    arena->width = width;
    arena->height = height;
}

void arena_print(struct Arena *arena){
    int i;
    for(i = 0; i < arena->width; i += 1) {
        mvaddch(0, i, ACS_CKBOARD);
    }

    for(i = 0; i < arena->height; i += 1) {
        mvaddch(i, 0, ACS_CKBOARD);
        mvaddch(i, arena->width, ACS_CKBOARD);
    }

    for(i = 0; i <= arena->width; i += 1) {
        mvaddch(arena->height, i, ACS_CKBOARD);
    }
}

