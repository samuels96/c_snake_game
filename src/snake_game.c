#include "arena.h"
#include "snake.h"
#include "fruit.h"
#include <stdio.h>
#include <stdlib.h>
#include <ncurses.h>
#include <time.h>
#include <unistd.h>

void cls(void){
    printf("\e[1;1H\e[2J");
}

int gotoxy(int x, int y){
    printf("%c[%d;%df", 0x1B, y, x);
}


int init_curses(void){
    initscr();
    cbreak();
    noecho();
    keypad(stdscr, TRUE); 
    curs_set(0); 
    timeout(50);
}


struct Fruit new_fruit(int x_bound, int y_bound){
    x_bound = rand() % (x_bound-1);
    if(x_bound == 0){
        x_bound += 2;
    }

    y_bound = rand() % (y_bound-1);
    if(y_bound == 0){
        y_bound += 2;
    }

    return fruit_factory(10, x_bound, y_bound);
}

char game_choice(void){
    int ch;
    while(1){
        ch = getch();
        switch(ch){
            case 10: //Enter key
               return 0;
            case 113: //q key
               return 1;
            default:
               break;
        }
    }
}



int snake_game(void){
    int arena_width = 50;
    int arena_height = 25;
    int score = 0;

    init_curses();

    struct Snake snake;
    struct Arena arena;
    struct Fruit fruit;

    arena_init(&arena, arena_width, arena_height);
    snake_init(&snake, arena_width, arena_height);

    arena_print(&arena);
    snake_print(&snake);

    srand(time(NULL));
    int snake_status;

    int ch;
    refresh();

    char score_str[50];
    enum Direction curr_dir = UP;
    fruit = new_fruit(arena_width, arena_height);

    do{

        ch = getch();
        
        switch(ch){
            case KEY_UP:
                if(curr_dir != DOWN){
                    curr_dir = UP;
                }
               break;
            case KEY_DOWN:
                if(curr_dir != UP){
                    curr_dir = DOWN;
                }
               break;
            case KEY_LEFT:
                if(curr_dir != RIGHT){
                    curr_dir = LEFT;
                }
               break;
            case KEY_RIGHT:
                if(curr_dir != LEFT){
                    curr_dir = RIGHT;
                }
               break;
        }

        erase();
        fruit_print(&fruit);
        arena_print(&arena);
        snake_print(&snake);

        snake_status = snake_move(&snake, curr_dir, arena_width, arena_height);

        if(snake_status == -1){
            snake_print_crash(&snake);
            return game_choice();
        }
        else if(snake_status == 1){
            score += fruit.pts;
            fruit = new_fruit(arena_width, arena_height);
        }

        snprintf(score_str, 50, "Score:  %d", score);
        mvprintw(arena_height+1, 0, score_str);

        refresh();
    }while(1);

    endwin();
    return 0;
}

int main(void){
    while(1){
        if(snake_game() == 1){
            break;
        }
    }
    return 0;
}
